#include "Giggles/Graphics/Display.h"

#include "Giggles/Debug.h"

namespace ggl {

    void Display::init(int width, int height)
    {
        glEnable(GL_BLEND);
    }

    int Display::getWidth()
    {
        int coordinates[4];
        glGetIntegerv(GL_VIEWPORT, coordinates);
        return coordinates[2];
    }

    int Display::getHeight()
    {
        int coordinates[4];
        glGetIntegerv(GL_VIEWPORT, coordinates);
        return coordinates[3];
    }
    float Display::getWidth_f() 
    { 
        int coordinates[4];
        glGetIntegerv(GL_VIEWPORT, coordinates);
        return (float)coordinates[2];
    }

    float Display::getHeight_f()
    { 
        int coordinates[4];
        glGetIntegerv(GL_VIEWPORT, coordinates);
        return (float)coordinates[3];
    }

} /* namespace ggl */