#include "Giggles/Graphics/RenderSystem.h"



namespace ggl {

    void RenderSystem::clearColor(float r, float g, float b, float a)
    {
        glClearColor(r, g, b, a);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    }

    void RenderSystem::clearColor(glm::vec4 color)
    {
        clearColor(color.r, color.g, color.b, color.a);
    }

    void RenderSystem::pushData(glm::mat4 current_transform,
         std::vector<float>& vertex_data,
         std::vector<unsigned short>& index_data)
    {
        for (std::vector<float>::size_type i = 0
            ; i < vertex_data.size()
            ; i += 8)
        {
            glm::vec4 res = projection
                * current_transform
                * glm::vec4(vertex_data.at(i), vertex_data.at(i+1),
                0.0f, 1.0f);

            vertex_buffer.push_back(res.x);
            vertex_buffer.push_back(res.y);

            vertex_buffer.insert(
                vertex_buffer.end(),
                vertex_data.begin() + i + 2,
                vertex_data.begin() + i + 8);


        }

        for (unsigned short index : index_data) {
            index_buffer.push_back(index_offset + index);
        }

        index_offset += 4;
    }

    void RenderSystem::setShader(ShaderResource & shad)
    {
        if (custom_shader != &shad) {
            if (vertex_buffer.size() > 0)
                render();
            custom_shader = &shad;
            custom_shader->enable();
            custom_shader->setProjection(projection);
            custom_shader->setTexture(current_texture);
        }
    }

    void RenderSystem::setTexture(GLuint texture_id)
    {
        if (current_texture != texture_id) {
            if (vertex_buffer.size() > 0)
                render();
            current_texture = texture_id;
            glBindTexture(GL_TEXTURE_2D, current_texture);
            if (custom_shader != nullptr) {
                custom_shader->setTexture(current_texture);
            }
            shader->setTexture(current_texture);
        }
    }

    //void RenderSystem::pushTransform(glm::mat4 transform)
    //{
    //    transform_buffer.push_back(transform);
    //}

    void RenderSystem::render()
    {


        // Bind buffer and link data
        glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer_obj);
        glBufferData(GL_ARRAY_BUFFER,
            vertex_buffer.size() * sizeof(float),
            &vertex_buffer.front(),
            GL_STATIC_DRAW);

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, index_buffer_obj);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER,
            index_buffer.size() * sizeof(unsigned short),
            &index_buffer.front(),
            GL_STATIC_DRAW
            );

        // Data layout
        pos_attribute = glGetAttribLocation(shader->getProgram(),
            "position");
        glEnableVertexAttribArray(pos_attribute);
        glVertexAttribPointer(pos_attribute, 2, GL_FLOAT,
            GL_FALSE, 8 * sizeof(GL_FLOAT), 0);

        color_attribute = glGetAttribLocation(shader->getProgram(),
            "color");
        glEnableVertexAttribArray(color_attribute);
        glVertexAttribPointer(color_attribute, 4, GL_FLOAT,
            GL_FALSE, 8 * sizeof(GL_FLOAT),
            (void*)(2 * sizeof(GL_FLOAT)));

        uv_attribute = glGetAttribLocation(shader->getProgram(),
            "tex_coords");
        glEnableVertexAttribArray(uv_attribute);
        glVertexAttribPointer(uv_attribute, 2, GL_FLOAT,
            GL_FALSE, 8 * sizeof(GL_FLOAT),
            (void*)(6 * sizeof(GL_FLOAT)));

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

        // Draw
        glDrawElements(GL_TRIANGLES,
            index_buffer.size(),
            GL_UNSIGNED_SHORT,
            nullptr);

        vertex_buffer.clear();
        index_buffer.clear();
        index_offset = 0;

        if (custom_shader != nullptr) {
            custom_shader = nullptr;
            shader->enable();
            shader->setProjection(projection);
            shader->setTexture(current_texture);
        }
    }
}
