#include "Giggles/Sensors/Accelerator.h"

#include <cmath>

namespace ggl {
    glm::vec3 Accelerator::getNormal()
    {
        float norm = std::sqrt(x*x + y*y + z*z);
        return glm::vec3(x / norm, y / norm, z / norm);
    }

    float Accelerator::getInclination()
    {
        return std::acos(getNormal().z);
    }

    float Accelerator::getRotation()
    {
        return std::atan2(getNormal().x, getNormal().y);
    }
}