#include "Giggles/Audio/AudioObject.h"

#include <assert.h>

namespace ggl {

    AudioObject* AudioObject::create(const std::string& filename, bool loop)
    {
        // TODO: Error checking
        AudioObject* value = new AudioObject;
        SLresult result;

        // create engine
        result = slCreateEngine(
            &value->engine_obj,
            0,
            nullptr,
            0,
            nullptr,
            nullptr);
        assert(SL_RESULT_SUCCESS == result);
        (void)result;

        // realize the engine
        result = (*value->engine_obj)->Realize(
            value->engine_obj,
            SL_BOOLEAN_FALSE);
        assert(SL_RESULT_SUCCESS == result);
        (void)result;

        // get the engine interface, which is needed in order to create other
        // objects
        result = (*value->engine_obj)->GetInterface(
            value->engine_obj,
            SL_IID_ENGINE,
            &value->engine);
        assert(SL_RESULT_SUCCESS == result);
        (void)result;

        // create output mix
        result = (*value->engine)->CreateOutputMix(
            value->engine,
            &value->output_mix_obj,
            0,
            nullptr,
            nullptr);
        assert(SL_RESULT_SUCCESS == result);
        (void)result;

        // realize the output mix
        result = (*value->output_mix_obj)->Realize(
            value->output_mix_obj,
            SL_BOOLEAN_FALSE);
        assert(SL_RESULT_SUCCESS == result);
        (void)result;

        // Open resource
        AudioResource* audio =
            ResourceManager::instance().open<AudioResource>(filename);

        SLDataLocator_AndroidFD loc_fd = {
            SL_DATALOCATOR_ANDROIDFD,
            audio->file_descriptor,
            audio->start,
            audio->length
        };
        SLDataFormat_MIME format_mime = {
            SL_DATAFORMAT_MIME,
            nullptr,
            SL_CONTAINERTYPE_UNSPECIFIED
        };
        SLDataSource audio_src = {&loc_fd, &format_mime};

        SLDataLocator_OutputMix loc_outmix = {
            SL_DATALOCATOR_OUTPUTMIX,
            value->output_mix_obj
        };
        SLDataSink audio_sink = {&loc_outmix, nullptr};

        // create player
        const SLInterfaceID ids[3] = {
            SL_IID_SEEK,
            SL_IID_MUTESOLO,
            SL_IID_VOLUME
        };
        const SLboolean req[3] = {
            SL_BOOLEAN_TRUE,
            SL_BOOLEAN_TRUE,
            SL_BOOLEAN_TRUE
        };
        result = (*value->engine)->CreateAudioPlayer(
            value->engine,
            &value->player_obj,
            &audio_src,
            &audio_sink,
            3,
            ids,
            req);
        assert(SL_RESULT_SUCCESS == result);
        (void)result;

        result = (*value->player_obj)->Realize(
            value->player_obj,
            SL_BOOLEAN_FALSE);
        assert(SL_RESULT_SUCCESS == result);
        (void)result;

        result = (*value->player_obj)->GetInterface(
            value->player_obj,
            SL_IID_PLAY,
            &value->player_play);
        assert(SL_RESULT_SUCCESS == result);
        (void)result;

        result = (*value->player_obj)->GetInterface(
            value->player_obj,
            SL_IID_SEEK,
            &value->player_seek);
        assert(SL_RESULT_SUCCESS == result);
        (void)result;

        result = (*value->player_obj)->GetInterface(
            value->player_obj,
            SL_IID_MUTESOLO,
            &value->player_mutesolo);
        assert(SL_RESULT_SUCCESS == result);
        (void)result;

        result = (*value->player_obj)->GetInterface(
            value->player_obj,
            SL_IID_VOLUME,
            &value->player_volume);
        assert(SL_RESULT_SUCCESS == result);
        (void)result;

        if (loop) {
            // Loooooop
            result = (*value->player_seek)->SetLoop(
                value->player_seek,
                SL_BOOLEAN_TRUE,
                0,
                SL_TIME_UNKNOWN);
            assert(SL_RESULT_SUCCESS == result);
            (void)result;
        }


        return value;
    }

    bool AudioObject::play()
    {
        SLresult result;

        if (playing) {
            return true;
        }

        if (player_play) {

            result = (*player_play)->SetPlayState(
                player_play,
                SL_PLAYSTATE_PLAYING);
            
            if (SL_RESULT_SUCCESS == result) {
                playing = true;
                return true;
            }
            
            else {
                return false;
            }
        }

        return false;

    }

    bool AudioObject::pause()
    {
        SLresult result;

        if (!playing) {
            return true;
        }

        if (player_play) {
            result = (*player_play)->SetPlayState(
                player_play,
                SL_PLAYSTATE_PAUSED);
            if (SL_RESULT_SUCCESS == result) {
                playing = false;
                return true;
            }
            else {
                return false;
            }
        }

        return false;
    }

    bool AudioObject::stop()
    {
        SLresult result;

        if (!playing) {
            return true;
        }

        if (player_play) {
            result = (*player_play)->SetPlayState(
                player_play,
                SL_PLAYSTATE_STOPPED);
            if (SL_RESULT_SUCCESS == result) {
                result = (*player_seek)->SetPosition(player_seek, 0, SL_SEEKMODE_FAST);
                playing = false;
                return true;
            }
            else {
                return false;
            }
        }

        return false;
    }

    AudioObject::~AudioObject()
    {
        if (player_obj)
            (*player_obj)->Destroy(player_obj);
        if (output_mix_obj)
            (*output_mix_obj)->Destroy(output_mix_obj);
        if (engine_obj)
            (*engine_obj)->Destroy(engine_obj);

        playing = false;

        engine_obj = nullptr;
        engine = nullptr;

        output_mix_obj = nullptr;

        player_obj = nullptr;
        player_play = nullptr;
        player_seek = nullptr;
        player_mutesolo = nullptr;
        player_volume = nullptr;
    }

} /* namespace ggl */
