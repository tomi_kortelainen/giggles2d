#include "Giggles/Debug.h"

#include "Giggles/Resources/ShaderResource.h"

namespace ggl {

    bool ShaderResource::load(const std::string& vert_filename,
                      const std::string& frag_filename)
    {
        std::ifstream vertex_file(vert_filename);
        std::ifstream fragment_file(frag_filename);

        std::stringstream vertex_buffer;
        std::stringstream fragment_buffer;


        vertex_shader = glCreateShader(GL_VERTEX_SHADER);
        fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);

        bool return_value = false;
        GLint result = GL_FALSE;
        int info_length;

        // Compile vertex shader
        vertex_buffer << vertex_file.rdbuf();
        const char* vert_src_ptr = vertex_buffer.str().c_str();
        glShaderSource(vertex_shader, 1, &vert_src_ptr, nullptr);
        glCompileShader(vertex_shader);

        glGetShaderiv(vertex_shader, GL_COMPILE_STATUS, &result);
        glGetShaderiv(vertex_shader, GL_INFO_LOG_LENGTH, &info_length);
        std::vector<char> vertex_error(info_length);
        glGetShaderInfoLog(
            vertex_shader,
            info_length,
            nullptr,
            &vertex_error[0]);
        Debug::Log(Debug::Level::ERROR, "%s\n", &vertex_error[0]);

        if (result == GL_FALSE)
            return_value = false;

        // Compile fragment shader
        fragment_buffer << fragment_file.rdbuf();
        const char* frag_src_ptr = fragment_buffer.str().c_str();
        glShaderSource(fragment_shader, 1, &frag_src_ptr, nullptr);
        glCompileShader(fragment_shader);

        glGetShaderiv(fragment_shader, GL_COMPILE_STATUS, &result);
        glGetShaderiv(fragment_shader, GL_INFO_LOG_LENGTH, &info_length);
        std::vector<char> fragment_error(info_length);
        glGetShaderInfoLog(
            fragment_shader,
            info_length,
            nullptr,
            &fragment_error[0]);
        Debug::Log(Debug::Level::ERROR, "%s\n", &fragment_error[0]);

        if (result == GL_FALSE)
            return_value = false;

        program_id = glCreateProgram();
        glAttachShader(program_id, vertex_shader);
        glAttachShader(program_id, fragment_shader);
        glLinkProgram(program_id);

        glGetProgramiv(program_id, GL_LINK_STATUS, &result);
        glGetProgramiv(program_id, GL_INFO_LOG_LENGTH, &info_length);
        std::vector<char> program_error(std::max(info_length, int(1)));
        glGetProgramInfoLog(
            program_id,
            info_length,
            nullptr,
            &program_error[0]);
        Debug::Log(Debug::Level::ERROR, "%s\n", &program_error[0]);

        if (result == GL_FALSE)
            return_value = false;

        return return_value;
    }

    bool ShaderResource::load(const char* vert_src_ptr, const char* frag_src_ptr)
    {


        vertex_shader = glCreateShader(GL_VERTEX_SHADER);
        fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);

        bool return_value = false;
        GLint result = GL_FALSE;
        int info_length;

        // Compile vertex shader
        glShaderSource(vertex_shader, 1, &vert_src_ptr, nullptr);
        glCompileShader(vertex_shader);

        glGetShaderiv(vertex_shader, GL_COMPILE_STATUS, &result);
        glGetShaderiv(vertex_shader, GL_INFO_LOG_LENGTH, &info_length);
        std::vector<char> vertex_error(info_length);
        glGetShaderInfoLog(vertex_shader, info_length, nullptr, &vertex_error[0]);
        Debug::Log(Debug::Level::ERROR, "%s\n", &vertex_error[0]);

        if (result == GL_FALSE)
            return_value = false;

        // Compile fragment shader
        glShaderSource(fragment_shader, 1, &frag_src_ptr, nullptr);
        glCompileShader(fragment_shader);

        glGetShaderiv(fragment_shader, GL_COMPILE_STATUS, &result);
        glGetShaderiv(fragment_shader, GL_INFO_LOG_LENGTH, &info_length);
        std::vector<char> fragment_error(info_length);
        glGetShaderInfoLog(fragment_shader, info_length, nullptr, &fragment_error[0]);
        Debug::Log(Debug::Level::ERROR, "%s\n", &fragment_error[0]);

        if (result == GL_FALSE)
            return_value = false;

        program_id = glCreateProgram();
        glAttachShader(program_id, vertex_shader);
        glAttachShader(program_id, fragment_shader);
        glLinkProgram(program_id);

        glGetProgramiv(program_id, GL_LINK_STATUS, &result);
        glGetProgramiv(program_id, GL_INFO_LOG_LENGTH, &info_length);
        std::vector<char> program_error(std::max(info_length, int(1)));
        glGetProgramInfoLog(program_id, info_length, nullptr, &program_error[0]);
        Debug::Log(Debug::Level::ERROR, "%s\n", &program_error[0]);

        if (result == GL_FALSE)
            return_value = false;

        return return_value;
    }

    void ShaderResource::setProjection(glm::mat4 proj)
    {
        projection = proj;
        glUniformMatrix4fv(projection_id, 1, GL_FALSE, glm::value_ptr(projection));
    }

    void ShaderResource::setTransformation(glm::mat4 trans)
    {
        transform = trans;
        glUniformMatrix4fv(transform_id, 1, GL_FALSE, glm::value_ptr(transform));
    }

    void ShaderResource::setTexture(GLuint texid)
    {
        texture_id = texid;
        glUniform1i(texture_id, 0);
    }

    void ShaderResource::enable()
    {
        glUseProgram(program_id);
        projection_id = glGetUniformLocation(program_id, "projection");
        transform_id = glGetUniformLocation(program_id, "transform");
    }

    GLuint ShaderResource::getProgram()
    {
        return program_id;
    }

    ShaderResource::ShaderResource()
    {
    }

    ShaderResource::ShaderResource(const std::string& vert_filename,
                   const std::string& frag_filename)
    {
        load(vert_filename, frag_filename);
    }


    ShaderResource::~ShaderResource()
    {
        glDeleteShader(vertex_shader);
        glDeleteShader(fragment_shader);
        if (program_id != 0) {
            glDeleteProgram(program_id);
            program_id = 0;
        }
    }

} /* namespace ggl */