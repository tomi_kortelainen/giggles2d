#include "Giggles/Resources/TextureResource.h"

#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>


namespace ggl {

    TextureResource::TextureResource()
    {
    }

    TextureResource::TextureResource(const std::string& filename)
    {
        AAssetManager* am = ResourceManager::instance().asset_manager;
        AAsset* asset = AAssetManager_open(am, filename.c_str(), AASSET_MODE_BUFFER);

        //off_t start, length;
        unsigned char* imgbuff = (unsigned char*)AAsset_getBuffer(asset);
        unsigned int len = AAsset_getLength(asset);
        //int fd = AAsset_openFileDescriptor(asset, &start, &length);

        int iwidth, iheight, n;
        unsigned char* img = stbi_load_from_memory(imgbuff, len, &iwidth, &iheight, &n, 0);

        glGenTextures(1, &texture_id);
        glBindTexture(GL_TEXTURE_2D, texture_id);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, iwidth, iheight, 0, GL_RGBA, GL_UNSIGNED_BYTE, img);

        AAsset_close(asset);

        width = (float)iwidth;
        height = (float)iheight;
        
    }


    TextureResource::~TextureResource()
    {
        glDeleteTextures(1, &texture_id);
    }

} /* namespace ggl */
