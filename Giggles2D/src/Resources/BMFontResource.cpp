﻿#include "Giggles/Resources/BMFontResource.h"

namespace ggl {
    const std::string & BMFontResource::getTextureFileName()
    {
        return texture_file;
    }
    bool BMFontResource::load(const std::string& filename)
    {

        AAssetManager* am = ResourceManager::instance().asset_manager;
        AAsset* asset = AAssetManager_open(am, filename.c_str(), AASSET_MODE_UNKNOWN);


        if (nullptr == asset)
            return false;

        long fsize = AAsset_getLength(asset);
        char* fbuf = (char*)malloc(sizeof(char)*fsize);
        AAsset_read(asset, fbuf, fsize);
        AAsset_close(asset);

        std::stringstream fntfile;

        fntfile << fbuf;

        std::string line;
        std::string read;
        std::string item;
        std::string val;
        std::size_t i;

        while (!fntfile.eof()) {
            std::stringstream sstream;
            std::getline(fntfile, line);
            sstream << line;

            sstream >> read;
            if (read == "common") {
                while (!sstream.eof()) {
                    std::stringstream convert;
                    sstream >> read;
                    i = read.find('=');
                    item = read.substr(0, i);
                    val = read.substr(i + 1);

                    convert << val;
                    if (item == "lineHeight") {
                        convert >> lineHeight;
                    }
                    else if (item == "base") {
                        convert >> base;
                    }
                    else if (item == "scaleW") {
                        convert >> scaleW;
                    }
                    else if (item == "scaleH") {
                        convert >> scaleH;
                    }
                    else if (item == "pages") {
                        convert >> pages;
                    }
                    else if (item == "packed") {
                        convert >> packed;
                    }
                    else if (item == "alphaChnl") {
                        convert >> alphaChnl;
                    }
                    else if (item == "redChnl") {
                        convert >> redChnl;
                    }
                    else if (item == "greenChnl") {
                        convert >> greenChnl;
                    }
                    else if (item == "blueChnl") {
                        convert >> blueChnl;
                    }

                }
            }
            else if (read == "page") {
                while (!sstream.eof()) {
                    std::stringstream convert;
                    sstream >> read;
                    i = read.find('=');
                    item = read.substr(0, i);
                    val = read.substr(i + 1);

                    convert << val;
                    if (item == "id") {
                        convert >> page_id;
                    }
                    else if (item == "file") {
                        convert >> texture_file;
                        while (texture_file.back() != '"') {
                            sstream >> read;
                            std::stringstream convert;
                            val = read.substr(0);
                            texture_file += " " + val;
                        }
                        texture_file = texture_file.substr(1, texture_file.size() - 2);
                    }
                }
            }
            else if (read == "char") {
                int charid = 0;
                BMFont_char c;

                while (!sstream.eof()) {
                    std::stringstream convert;
                    sstream >> read;
                    i = read.find('=');
                    item = read.substr(0, i);
                    val = read.substr(i + 1);

                    convert << val;
                    if (item == "id") {
                        convert >> charid;
                    }
                    else if (item == "x") {
                        convert >> c.x;
                    }
                    else if (item == "y") {
                        convert >> c.y;
                    }
                    else if (item == "width") {
                        convert >> c.width;
                    }
                    else if (item == "height") {
                        convert >> c.height;
                    }
                    else if (item == "xoffset") {
                        convert >> c.xoffset;
                    }
                    else if (item == "yoffset") {
                        convert >> c.yoffset;
                    }
                    else if (item == "xadvance") {
                        convert >> c.xadvance;
                    }
                    else if (item == "page") {
                        convert >> c.page;
                    }
                    else if (item == "chnl") {
                        convert >> c.chnl;
                    }
                }
                char_map.insert(std::map<int, BMFont_char>::value_type(charid, c));
            }
            else if (read == "kernings") {
                while (!sstream.eof()) {
                    std::stringstream convert;
                    sstream >> read;
                    i = read.find('=');
                    item = read.substr(0, i);
                    val = read.substr(i + 1);

                    convert << val;
                    if (item == "count") {
                        convert >> kernings;
                    }
                }
            }
            else if (read == "kerning") {
                BMFont_kerning k;

                while (!sstream.eof()) {
                    std::stringstream convert;
                    sstream >> read;
                    i = read.find('=');
                    item = read.substr(0, i);
                    val = read.substr(i + 1);

                    convert << val;
                    if (item == "first") {
                        convert >> k.first;
                    }
                    else if (item == "second") {
                        convert >> k.second;
                    }
                    else if (item == "amount") {
                        convert >> k.amount;
                    }
                }

            }

        }

        return true;
    }

    BMFont_char& BMFontResource::getChar(int chara)
    {
        return char_map[chara];
    }

    float BMFontResource::getKerning(int first, int second)
    {
        if (kerning_pairs.size() > 0) {
            for (int i = 0; i < kerning_pairs.size(); i++) {
                if (kerning_pairs[i].first == first && kerning_pairs[i].second == second) {
                    return (float)kerning_pairs[i].amount;
                }
            }
        }
        return 0.f;
    }

    BMFontResource::BMFontResource()
    {
    }

    BMFontResource::BMFontResource(const std::string& filename)
    {
        load(filename);
    }


    BMFontResource::~BMFontResource()
    {
    }

} /* namespace ggl */