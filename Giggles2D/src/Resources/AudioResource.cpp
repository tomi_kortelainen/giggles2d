#include "Giggles/Resources/AudioResource.h"

namespace ggl {

    void AudioResource::load(const std::string& filename)
    {
        // TODO: Error checking
        AAssetManager* am = ResourceManager::instance().asset_manager;
        AAsset* asset = AAssetManager_open(am, filename.c_str(), AASSET_MODE_UNKNOWN);
        
        file_descriptor = AAsset_openFileDescriptor(asset, &start, &length);
        AAsset_close(asset);
    }

    AudioResource::AudioResource(){}

    AudioResource::AudioResource(const std::string& filename)
    {
        load(filename);
    }

    AudioResource::~AudioResource()
    {

    }

}