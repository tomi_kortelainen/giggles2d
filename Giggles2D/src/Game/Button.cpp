#include "giggles/Game/GUI/Button.h"

namespace ggl {

    Button::Button() : 
        transform(addComponent<Transform>("transform")),
        rectangle(addComponent<Rectangle>("rectangle")),
        texture(addComponent<Texture>("texture")),
        render(addComponent<RenderShape>("rendershape"))
    {
    }

    Button::Button(const std::wstring& text, float x, float y, float width, float height, float border_width) :
        transform(addComponent<Transform>("transform")),
        rectangle(addComponent<Rectangle>("rectangle")),
        texture(addComponent<Texture>("texture")),
        render(addComponent<RenderShape>("rendershape"))
    {
        transform.position = { x, y };
        rectangle.setDimensions(width, height);
        for (int x = 0; x < 3; x++) {
            for (int y = 0; y < 3; y++) {
                rects[x][y] = new Rectangle();
            }
        }
    }

    Button::~Button()
    {
    }

}