#include "Giggles/Game/App.h"

namespace ggl {


    void App::run()
    {
        running = true;
    }

    void App::update()
    {
        current_scene->update();
    }

    void App::startScene(Scene* scene)
    {
        if (current_scene != nullptr) {
            delete current_scene;
        }

        current_scene = scene;
    }

    App::App()
    {

    }

    App::~App()
    {

    }

} /* namespace ggl */