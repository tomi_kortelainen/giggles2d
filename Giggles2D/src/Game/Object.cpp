
#include "Giggles/Game/Object.h"

namespace ggl {

    Component* Object::addComponent(const std::string& name,
                                    Component* component)
    {
        components.emplace_back(component);
        components.back()->name = name;

        return components.back().get();
    }

    Component* Object::operator[] (const std::string& name) &
    {
        for (auto&& component : components) {
            if (component->name == name)
                return component.get();
        }
        return nullptr;
    }

    Component* Object::getComponent(const std::string& name)
    {
        for (auto&& component : components) {
            if (component->name == name) {
                return component.get();
            }
        }
        return nullptr;
    }

    void Object::update()
    {
        //for (auto&& component : components) {

        //}
    }

    Object::Object()
    {
    }

    Object::Object(const std::string& name)
    {
        this->name = name;
    }


    Object::~Object()
    {
        //for (auto&& component : components)
        //{
        //    delete component;
        //}
    }

} /* namespace ggl */