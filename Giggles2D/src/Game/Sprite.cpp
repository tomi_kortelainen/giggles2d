#include "Giggles/Game/Sprite.h"

namespace ggl {

    Sprite::Sprite() :
        transform(addComponent<Transform>("transform")),
        rectangle(addComponent<Rectangle>("rectangle")),
        texture(addComponent<Texture>("texture")),
        render(addComponent<RenderShape>("rendershape"))
    {
    
    }
    
    Sprite::~Sprite()
    {
    }

} /* namespace ggl */
