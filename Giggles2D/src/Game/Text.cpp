﻿#include "Giggles/Game/GUI/Text.h"

namespace ggl {
    Text::Text(const std::string& fontfile) :
        transform(addComponent<Transform>("transform")),
        rectangle(addComponent<Rectangle>("rectangle")),
        render(addComponent<RenderShape>("rendershape")),
        shader(addComponent<Shader>("shader")),
        texture(addComponent<Texture>("texture"))
    {
        font.load(fontfile);
        texture.load("assets/" + font.getTextureFileName());
    }

    Text::Text(const std::string& fontfile, const std::wstring& msg) :
        transform(addComponent<Transform>("transform")),
        rectangle(addComponent<Rectangle>("rectangle")),
        render(addComponent<RenderShape>("rendershape")),
        shader(addComponent<Shader>("shader")),
        texture(addComponent<Texture>("texture"))
    {
        text = msg;
        font.load(fontfile);
        texture.load(font.getTextureFileName());
        shader.load(vertex_shader, fragment_shader);

        updateText();
    }

    Text::~Text()
    {
    }

    void Text::updateText()
    {
        /*
        First four components are common for all the rest. After that comes the
        components for the letters and those are the ones that needs to be replaced.
        */
        components.erase(components.begin() + 5, components.end());
        float cur_x = transform.position.x;
        for (int i = 0; i < text.length(); i++) {

            BMFont_char& let = font.getChar(text[i]);
            //Debug::Log(Debug::Level::INFO, "%f, %f, %f, %f, %f, %f, %f", let.x, let.y, let.xadvance, let.xoffset, let.yoffset, let.width, let.height);

            Transform& letrans = addComponent<Transform>("transform");
            letrans.setPosition(cur_x + let.xoffset, transform.position.y + let.yoffset);
            cur_x += let.xadvance;
            if (text.length() > 1 && i < text.length()) {
                cur_x += font.getKerning(text[i], text[i + 1]);
            }

            Rectangle& letrect = addComponent<Rectangle>("rect", let.width, let.height);
            letrect.setTextureCoordinates(
                let.x / texture.width,
                let.y / texture.height,
                let.width / texture.width,
                let.height / texture.height);
            
            letters.push_back({ letrans, letrect });

        }
    }

} /* namespace ggl */