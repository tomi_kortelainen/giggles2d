#include "Giggles/Game/Scene.h"
#include "Giggles/Components/Transform.h"
#include "Giggles/Components/Shader.h"
namespace ggl {

    void Scene::update()
    {
        glClearColor(1.0f, 0.0f, 1.0f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        for (auto&& object : objects) {

            bool visible = false;
            //std::vector<float>* tmp_vert = nullptr;
            //std::vector<unsigned short>* tmp_ind = nullptr;
            //glm::mat4 tmp_transform;
            std::vector<std::vector<float>*> tmp_vert;
            std::vector<std::vector<unsigned short>*> tmp_ind;
            std::vector<glm::mat4> tmp_transform;


            GLuint tmp_texture = 0;

            for (auto&& component : object->components) {

                switch (component->getType()) {

                    case BIType::TRANSFORM:
                    {
                        tmp_transform.push_back(
                            ((Transform*)component.get())->getTransform());
                        break;
                    }
                    case BIType::SHAPE:
                    {
                        tmp_vert.push_back(&((Shape*)component.get())->vertices);
                        tmp_ind.push_back(&((Shape*)component.get())->indices);
                        break;
                    }
                    case BIType::TEXTURE:
                    {
                        tmp_texture = ((Texture*)component.get())->getTexture();
                        break;
                    }
                    case BIType::RENDER_SHAPE:
                    {
                        visible = true;
                        break;
                    }
                    case BIType::SHADER:
                    {
                        render.setShader(*((Shader*)component.get())->shader);
                        break;
                    }

                    default:
                    {
                        break;
                    }

                }

            }

            //if (visible && tmp_vert != nullptr) {
            //    render.setTexture(tmp_texture);
            //    render.setTransform(tmp_transform);
            //    render.pushData(*tmp_vert, *tmp_ind);
            //}

            if (visible) {
                render.setTexture(tmp_texture);
                //ggl::Debug::Log(ggl::Debug::Level::INFO, "%d, %d, %d", tmp_vert.size(), tmp_ind.size(), tmp_transform.size());
                for (int i = 0; i < tmp_vert.size(); i++) {
                    //render.pushTransform(tmp_transform[i]);
                    render.pushData(tmp_transform[i] , *tmp_vert[i], *tmp_ind[i]);
                }
            }

            object->update();
            if (render.custom_shader != nullptr) {
                render.render();
            }
            
        }
        render.render();
    }

    Object* Scene::addObject(const std::string& name)
    {
        objects.emplace_back(new Object(name));
        return objects.back().get();
    }

    Object* Scene::addObject(Object* object)
    {
        objects.emplace_back(object);
        return objects.back().get();
    }

    Object* Scene::getObject(const std::string& name)
    {
        for (auto&& item : objects) {
            if (item->name == name) {
                return item.get();
            }
        }
        return nullptr;
    }

    void Scene::removeObject(const std::string& name)
    {
        for (int i = 0; i < objects.size(); i++) {
            if (objects[i]->name == name) {
                objects.erase(objects.begin() + i);
            }
        }
    }

    Scene::Scene()
    {
    }


    Scene::~Scene()
    {
        objects.clear();
    }

} /* namespace ggl */