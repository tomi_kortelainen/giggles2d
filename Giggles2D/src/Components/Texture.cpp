#include "Giggles/Components/Texture.h"
#include "Giggles/Resources/ResourceManager.h"
#include "Giggles/Resources/TextureResource.h"

namespace ggl {
    
    GLuint Texture::getTexture()
    {
        return texture_id;
    }

    float Texture::getTextureX(float pixelcoordx)
    {
        return pixelcoordx / width;
    }

    float Texture::getTextureY(float pixelcoordy)
    {
        return pixelcoordy / height;
    }

    Texture::Texture()
    {
    }

    Texture::Texture(const std::string& filename)
    {
        load(filename);
    }

    void Texture::load(const std::string& filename)
    {

        texture_id = ResourceManager::instance()
            . open<TextureResource>(filename)
            ->texture_id;
        width = ResourceManager::instance()
            .open<TextureResource>(filename)
            ->width;
        height = ResourceManager::instance()
            .open<TextureResource>(filename)
            ->height;

    }


    Texture::~Texture()
    {
    }

} /* namespace ggl */
