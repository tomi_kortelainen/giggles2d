#include "Giggles/Components/Shader.h"

namespace ggl {

    bool Shader::load(const char* vert_ptr, const char* frag_ptr)
    {
        shader = new ShaderResource();
        return shader->load(vert_ptr, frag_ptr);
    }

    void Shader::setTexture(GLuint texid)
    {
        shader->setTexture(texid);
    }

    void Shader::setProjection(glm::mat4 proj)
    {
        shader->setProjection(proj);
    }

    void Shader::enable()
    {
        shader->enable();
    }

    GLuint Shader::getProgram()
    {
        return shader->getProgram();

    }

    Shader::Shader()
    {
    }

    Shader::Shader(const char* vert_src, const char* frag_src)
    {
        load(vert_src, frag_src);
    }
    Shader::~Shader()
    {
    }

}