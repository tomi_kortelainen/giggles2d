
#include "Giggles/Components/Transform.h"

namespace ggl{

    void Transform::setRotation(float angle)
    {
        rotation = angle;
    }

    void Transform::setScale(float x, float y)
    {
        scale.x = x;
        scale.y = y;
    }

    void Transform::setPosition(float x, float y)
    {
        // TODO: check if correct...
        position.x = x - origin.x;
        position.y = y - origin.y;
    }

    void Transform::setOrigin(float x, float y)
    {
        origin.x = x;
        origin.y = y;
    }

    glm::mat4 Transform::getTransform()
    {

        glm::mat4 scalemat = glm::scale(
            identity,
            glm::vec3(scale, 1.0));

        glm::mat4 rotmat = glm::rotate(
            identity,
            rotation, glm::vec3(0.0f, 0.0f, 1.0f));

        glm::mat4 transmat = glm::translate(
            identity,
            glm::vec3(position, 0.0f));

        return glm::translate(identity, glm::vec3(origin, 0.0f))
             * transmat
             * rotmat
             * scalemat
             * glm::translate(identity, glm::vec3(-origin, 0.0f));
    }

    Transform::Transform()
    {
    }

    Transform::Transform(float x, float y)
    {
        position.x = x;
        position.y = y;
    }

    Transform::~Transform()
    {
    }

}/* namespace ggl */