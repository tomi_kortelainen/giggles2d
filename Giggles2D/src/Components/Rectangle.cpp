#include "Giggles/Components/Rectangle.h"

namespace ggl {


    void Rectangle::setDimensions(float width, float height)
    {
        this->width = width;
        this->height = height;
        createVertices(width, height);
    }

    void Rectangle::setTextureCoordinates(float x, float y, float width, float height)
    {
        texture_x = x;
        texture_y = y;
        texture_w = width;
        texture_h = height;
        createVertices(this->width, this->height);
    }

    Rectangle::Rectangle()
    {
        createVertices(width, height);
    }
    Rectangle::Rectangle(float width, float height)
    {
        this->width = width;
        this->height = height;
        createVertices(width, height);
    }

    Rectangle::Rectangle(const std::string& name)
    {
        this->name = name;
        createVertices(width, height);
    }
    Rectangle::Rectangle(const std::string& name, float width, float height)
    {
        this->name = name;
        this->width = width;
        this->height = height;
        createVertices(width, height);
    }

    //private
    void Rectangle::createVertices(float width, float height)
    {
        vertices.clear();
        indices.clear();
        float verts[] = {
            //Left Bottom
            0.0f, height,                  // position
            color.r, color.g, color.b, color.a,        // color
            texture_x, texture_y + texture_h,                    // uv

            //Left Top
            0.0f, 0.0f,              
            color.r, color.g, color.b, color.a,
            texture_x, texture_y,

            //Right Top
            width, 0.0f,
            color.r, color.g, color.b, color.a,
            texture_x + texture_w, texture_y,

            //Right Bottom
            width, height,
            color.r, color.g, color.b, color.a,
            texture_x + texture_w, texture_y + texture_h
        };


        vertices.insert(vertices.end(),
            verts,
            verts + (sizeof(verts) / sizeof(float)));

        indices.push_back(0);
        indices.push_back(1);
        indices.push_back(2);
        indices.push_back(0);
        indices.push_back(2);
        indices.push_back(3);
    }

    Rectangle::~Rectangle()
    {
    }

}
