#include "Giggles/Debug.h"

#include <EGL/egl.h>

#include <GLES2/gl2.h>

#include <cstdarg>

namespace ggl {

    void Debug::Log(Level tag, const char* message, ...)
    {
        va_list arglist;
        va_start(arglist, message);
        (void)__android_log_vprint((int)tag, "Giggles", message, arglist);
        va_end(arglist);
    }


    void Debug::CheckGLError(Level tag, const char* additional_message)
    {
        EGLint error = eglGetError();
        if (error != EGL_SUCCESS)
        {
            Log(tag, "%s: EGLError %d\n", additional_message, error);

        }
    }
} /* namespace ggl */