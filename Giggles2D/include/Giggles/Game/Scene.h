#pragma once

#include <string>
#include <unordered_map>
#include <memory>

#include <GLES2/gl2.h>

#include "Giggles/Debug.h"
#include "Giggles/Game/Object.h"
#include "Giggles/Graphics/RenderSystem.h"
#include "Giggles/Components/Texture.h"
#include "Giggles/Components/Shape.h"

#include "Giggles/Input/Touch.h"


namespace ggl {

    /**
    Scene contains list of objects.
    */
    class Scene
    {
    public:
        
        /**
        Gets called at every frame. Calls update methods of every object and
        checks for built in components.
        */
        virtual void update();

        /**
        Add object to the scene. Constructs new object.

        @param  name    Name for the object.
        @return Pointer to the added object.
        */
        Object* addObject(const std::string& name);

        /**
        Add existing object to the scene.

        @param  object  Pointer to the object.
        @return Pointer back to the added object.
        */
        Object* addObject(Object* object);

        /**
        Get object in the scene.

        @param  name    Name of the object to get.
        @return Pointer to the requested object. nullptr if doesn't exist.
        */
        Object* getObject(const std::string& name);

        /**
        Removes object in the scene.

        @param  name    Name of the object to remove.
        */
        void removeObject(const std::string& name);

        /**
        Default contructor.
        */
        Scene();
        virtual ~Scene();

    protected:
        
        /**
        Reference to the RenderSystem instance. Interface to drawing functions.
        */
        RenderSystem& render = RenderSystem::instance();

        /**
        Reference to the Touch instance. Interface to the touch input state.
        */
        Touch& touch = Touch::instance();


    private:
        /**
        Vector containing unique pointers to the objects in the scene.
        */
        std::vector<std::unique_ptr<Object>> objects;
    };

} /* namespace ggl */