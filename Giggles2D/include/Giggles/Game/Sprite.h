#pragma once

#include "Giggles/Game/Object.h"
#include "Giggles/Components/Transform.h"
#include "Giggles/Components/Rectangle.h"
#include "Giggles/Components/Texture.h"
#include "Giggles/Components/RenderShape.h"

namespace ggl {

    /**
    Convenience class that collects together common components needed by sprite
    object.
    */
    class Sprite : public Object
    {
        public:
            /**
            Transform component.
            */
            Transform&   transform;

            /**
            Rectangle component.
            */
            Rectangle&   rectangle;

            /**
            Texture component.
            */
            Texture&     texture;

            /**
            Render components. Indicates that object is drawable.
            */
            RenderShape& render;

            /**
            Default constructor.
            */
            Sprite();
            ~Sprite();
        private:
    };

} /* namespace ggl */
