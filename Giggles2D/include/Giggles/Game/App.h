#pragma once

#include "Giggles/Game/Object.h"
#include "Giggles/Game/Scene.h"

#include <string>
#include <unordered_map>

namespace ggl {

    /**
    App is a root class of your game or application. It contains pointer to the
    current scene and updates accordingly.
    */
    class App
    {

    public:


        void run();

        /**
        Updates application and current scene.
        */
        void update();

        /**
        Sets current scene.

        @param  scene   Pointer to the scene to play.
        */
        void startScene(Scene* scene);

        /**
        Default constructor.
        */
        App();
        ~App();

    private:
    bool running = false;
        Scene* current_scene = nullptr;

    };

} /* namespace ggl */