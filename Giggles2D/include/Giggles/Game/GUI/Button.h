#pragma once

#include "Giggles/Game/Object.h"
#include "Giggles/Components/Transform.h"
#include "Giggles/Components/Rectangle.h"
#include "Giggles/Components/Texture.h"
#include "Giggles/Components/RenderShape.h"

namespace ggl {

    class Button : public Object
    {
    public:

        /**
        Transform component.
        */
        Transform&   transform;

        /**
        Rectangle component.
        */
        Rectangle&   rectangle;

        /**
        Texture component.
        */
        Texture&     texture;

        /**
        Render components. Indicates that object is drawable.
        */
        RenderShape& render;

        void setBorder(float width);

        Button();
        Button(const std::wstring& text, float x, float y, float width, float height, float border_width);
        ~Button();

    private:
        Rectangle* rects[3][3];
    };

}
