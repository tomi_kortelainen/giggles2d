﻿#pragma once

#include <GLES2/gl2.h>

#include "Giggles/Game/Object.h"
#include "Giggles/Components/Transform.h"
#include "Giggles/Components/Rectangle.h"
#include "Giggles/Components/Texture.h"
#include "Giggles/Components/RenderShape.h"
#include "Giggles/Resources/BMFontResource.h"
#include "Giggles/Components/Shader.h"

namespace ggl {

    struct Letter
    {
        Transform& transform;
        Rectangle& rectangle;
    };

    class Text : public Object
    {
    public:
        
        /**
        Transform component.
        */
        Transform& transform;

        /**
        Rectangle component. Useless but needed...
        */
        Rectangle& rectangle;

        /**
        Render components. Indicates that object is drawable.
        */
        RenderShape& render;

        /**
        TODO: docs
        */
        Shader& shader;

        /**
        Public member containing text to display.
        */
        std::wstring text = L"";
        
        /**
        Sets the font the text is displayed with.

        @param  filename    Path to the font file.
        @return True if font was found and loading was succesfull, false
                otherwise.
        */
        //bool setFont(const std::string& filename);

        void updateText();

        /**
        Default constructor.
        */
        Text(const std::string& fontfile);

        //void update();

        /**
        Constructor taking text to display as parameter.

        @param  msg Text to display.
        */
        Text(const std::string& fontfile, const std::wstring& msg);
        ~Text();

    protected:

        std::vector<Letter> letters;

        BMFontResource font;

        /**
        Texture component.
        */
        Texture&  texture;

    private:
        const char* vertex_shader =
            "attribute mediump vec2 position;\n"
            "attribute mediump vec4 color;\n"
            "attribute mediump vec2 tex_coords;\n"

            "varying mediump vec4 out_color;\n"
            "varying mediump vec2 out_tex_coords;\n"

            "uniform mediump mat4 projection;\n"
            "//uniform mediump mat4 transform;\n"

            "void main(void)\n"
            "{\n"
            "    out_color = color;\n"
            "    out_tex_coords = tex_coords;\n"
            "    //gl_Position = projection * transform * vec4(position, 0.0, 1.0);\n"
            "    gl_Position = vec4(position, 0.0, 1.0);\n"
            "}\n";

        const char* fragment_shader =
            "varying mediump vec2 out_tex_coords;\n"
            "varying mediump vec4 out_color;\n"

            "uniform sampler2D texture;\n"

            "void main(void)\n"
            "{\n"
            "     mediump vec4 color = texture2D(texture, out_tex_coords);\n"
            "     gl_FragColor = vec4(out_color.r, out_color.g, out_color.b, color.a);\n"
            "    //gl_FragColor = out_color;\n"
            "}\n";
    };


} /* namespace ggl */