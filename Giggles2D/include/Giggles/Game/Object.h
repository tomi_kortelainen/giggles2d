#pragma once

#include <vector>
#include <string>
#include <memory>

#include "Giggles/Components/Component.h"
//#include "Giggles/Components/Transform.h"

namespace ggl {

    /**
    Base class for most of the objects in the game. Object has a list off
    components.
    */
    class Object
    {
    public:

        /**
        Name of the object. Optional.
        */
        std::string name = "";

        /**
        Operator overload for square brackets. Returns component with given
        name.
        
        Usage (example of getting texture):
            auto texture = (Texture*)object["texture"];
        
        @param  name    Name of the component.
        @return Pointer to the requested component. nullptr if not found.
        */
        Component* operator[] (const std::string& name) &;

        /**
        Add exsisting component to the object.

        @param  name        Name for the component.
        @param  component   Pointer to the component to add.
        @return Pointer to the added component.
        */
        Component* addComponent(const std::string& name, Component* component);

        /**
        Gets component.

        @param  name    Name of the component.
        @return Pointer to the component. nullptr if not found.
        */
        Component* getComponent(const std::string& name);

        /**
        Template method to add component to the object.

        Usage (example of adding texture):
            auto texture = object.addComponent<Texture>("texture", "image.png");

        @param  name    Name of the component.
        @param  args    Arguments that are passed to the components
                        constructor.
        @return Reference to the added component.
        */
        template <typename T, typename... Args>
        T& addComponent(const std::string& name, Args... args);

        /**
        Template method to get component.

        Usage (example of getting texture)
            auto texture = object.getComponent<Texture>("texture");

        @param  name    Name of the component.
        @return Pointer to the component. nullptr if not found.
        */
        template <typename T>
        T* getComponent(const std::string& name);

        /**
        Removes component.

        @param  name    Name of the component to remove.
        */
        bool removeComponent(const std::string& name);

        /**
        Virtual method that gets called at every frame.
        */
        virtual void update();

        /**
        Constructor
        */
        Object();

        /**
        Constructor

        @param  name    Name of the object
        */
        Object(const std::string& name);

        /**
        Destructor
        */
        virtual ~Object();

    //private:
        /**
        Vector containing pointers to child objects.
        */
        std::vector<std::unique_ptr<Component>> components;

    };

    // Templates

    template <typename T, typename... Args>
    T& Object::addComponent(const std::string& name, Args... args)
    {
        components.emplace_back(new T(args...));
        components.back()->name = name;
        return static_cast<T&>(*components.back().get());
    }

    template <typename T>
    T* Object::getComponent(const std::string& name)
    {
        for (auto&& component : components) {
            if (component->name == name)
                return static_cast<T*>(component.get());
        }
        return nullptr;
    }

} /* namespace ggl */
