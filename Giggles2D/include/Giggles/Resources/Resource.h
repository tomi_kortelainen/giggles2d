#pragma once

namespace ggl {

    /**
    Base class that all resource classes should inherit from.
    */
    class Resource
    {
    public:
        Resource();
        virtual ~Resource();
    };

} /* namespace ggl */