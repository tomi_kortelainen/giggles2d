#pragma once

#include <string>
#include <fstream>
#include <sstream>
#include <vector>

#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <GLES2/gl2.h>

#include "Giggles/Resources/Resource.h"

namespace ggl {

    /**
    Shader resource. Contains interface to handle shader related tasks.
    */
    class ShaderResource : public Resource
    {
    public:

        // TODO: implement.
        /**
        Load shader from file. (not implemented...)

        @param  vert_filename   Path to the vertex shader.
        @param  frag_filename   Path to the fragment shader.
        @return Returns true if success, false otherwise.
        */
        bool load(const std::string& vert_filename,
                  const std::string& frag_filename);

        /**
        Load shader from C style string in memory.

        @param  vert_ptr    Pointer to the vertex shader.
        @param  frag_ptr    Pointer to the fragment shader.
        */
        bool load(const char* vert_ptr, const char* frag_ptr);

        /**
        Sets texture to be used by shaders.

        @param  texid   Id of the texture to be used.
        */
        void setTexture(GLuint texid);

        /**
        Enables the shader.
        */
        void enable();

        // TODO: clean up! These are not currently needed. Should there be 
        // a more generic way to add uniforms?
        void setProjection(glm::mat4 proj);
        void setTransformation(glm::mat4 trans);

        /**
        Get the id of the shader program on the GPU.

        @return Id of the shader program.
        */
        GLuint getProgram();


        /**
        Default constructor.
        */
        ShaderResource();

        /**
        Constructor taking the filenames for the vertex and the fragment
        shaders. (not implemented yet...)

        @param  vert_filename   Path to the vertex shader.
        @param  frag_filename   Path to the fragment shader.
        */
        ShaderResource(const std::string& vert_filename,
               const std::string& frag_filename);
        ~ShaderResource();

    protected:
        GLuint vertex_shader = 0;
        GLuint fragment_shader = 0;
        GLuint program_id = 0;

        GLint projection_id;
        GLint transform_id;
        GLint texture_id;

        glm::mat4 projection;
        glm::mat4 transform;
    };

} /* namespace ggl */
