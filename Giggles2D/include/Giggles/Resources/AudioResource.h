#pragma once

#include <string>

#include "Giggles/Resources/ResourceManager.h"
#include "Giggles/Resources/Resource.h"

namespace ggl {

    /**
    Audio resource object that handles loading audiofile from apk and
    getting/storing its filedescriptor, length and starting point.
    */
    class AudioResource : public Resource
    {
        /**
        Grant AudioObject access to protected members and methods.
        */
        friend class AudioObject;
        public:
            
            /**
            Load audiofile.

            @param  filename    File to load.
            */
            void load(const std::string& filename);
            
            /**
            Default constructor.
            */
            AudioResource();
           
            /**
            Constructor to load file.

            @param  filename    File to load.
            */
            AudioResource(const std::string& filename);
            ~AudioResource(); 
        protected:
            int file_descriptor = 0;
            off_t start;
            off_t length;
    };

}
