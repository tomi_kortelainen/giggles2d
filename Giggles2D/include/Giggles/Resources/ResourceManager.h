#pragma once

//#include <jni.h>
#include <android/asset_manager.h>

#include <string>
#include <unordered_map>
#include <memory>
#include <utility>

#include "Giggles/Resources/Resource.h"


namespace ggl {

    /**
    Singleton class managing loaded resources.
    */
    class ResourceManager
    {
    public:

        /**
        AssetManager object from Java.
        */
        AAssetManager* asset_manager = nullptr;

        /**
        Opens resource and caches it for as long as it is needed.

        @param  filename    File to open.

        @return Pointer to the loaded resource.
        */
        template <typename T>
        T* open(const std::string& filename);

        /**
        Get instance of the singleton object.
        */
        static ResourceManager& instance()
        {
            static ResourceManager resource_manager;
            return resource_manager;
        }

    private:



        // Singleton
        ResourceManager(){};
        ~ResourceManager(){};

        // Non-copyable
        ResourceManager(const ResourceManager&) = delete;
        void operator=(const ResourceManager&) = delete;


        std::unordered_map<std::string, std::unique_ptr<Resource>> resources;
    };

    // Template definitions

    template <typename T>
    T* ResourceManager::open(const std::string& filename)
    {
        auto resource = resources.find(filename);
        if (resource != resources.end()) {
            return (T*)(resource->second.get());
        }
        else {
            std::unique_ptr<Resource> tmp(new T(filename));
            resources[filename] = std::move(tmp);
            return (T*)resources[filename].get();
        }

        
    }

} /* namespace ggl */
