#pragma once

#include <string>

#include <GLES2/gl2.h>

#include "Giggles/Resources/ResourceManager.h"
#include "Giggles/Resources/Resource.h"

namespace ggl {

    /**
    Texture resource object that handles loading texture file from apk and
    loading it to the GPU.
    */
    class TextureResource : public Resource
    {
    public:

        float width;
        float height;

        /**
        Id of the texture stored in video memory.
        */
        GLuint texture_id;

        /**
        Default constructor.
        */
        TextureResource();
        
        /**
        Constructor loading file with given name.

        @param  filename    Path to the file to load.
        */
        TextureResource(const std::string& filename);
        ~TextureResource();

    private:
        
    };

} /* namespace ggl */
