﻿#pragma once

#include <string>
#include <fstream>
#include <sstream>

#include <vector>
#include <map>

#include <cstdio>
#include <cstdlib>

#include <GLES2/gl2.h>

#include "Giggles/Resources/Resource.h"
#include "Giggles/Resources/TextureResource.h"

namespace ggl {

    /**
    Struct containing kerning info between two characters.
    */
    struct BMFont_kerning
    {
        int first;
        int second;
        float amount;

    };

    /**
    Struct containing character info relating to texture data.
    */
    struct BMFont_char
    {
        float x;
        float y;
        float width;
        float height;
        float xoffset;
        float yoffset;
        float xadvance;
        int page;
        int chnl;
    };

    /**
    Resource object that handles loading bitmap font as defined here:
        http://www.angelcode.com/products/bmfont/
    */
    class BMFontResource : public Resource
    {
    public:

        /**
        Returns filename of the texturefile containing glyphs.
        */
        const std::string& getTextureFileName();

        /**
        Loads font.

        @param  filename    Font file generated with bmfont generator.
        */
        bool load(const std::string& filename);

        /**
        Returns character info related to texture data.

        @param  chara   Character to get info about.
        */
        BMFont_char& getChar(int chara);

        /**
        Returns kerning between two characters.

        @param  first   First character.
        @param  second  Second character.
        */
        float getKerning(int first, int second);

        BMFontResource();

        /**
        Constructor that loads font.

        @param  filename    Font file generated with bmfont generator.
        */
        BMFontResource(const std::string& filename);
        ~BMFontResource();

    private:

        float scaleW;
        float scaleH;
        float lineHeight;
        float base;
        int pages;
        int packed;
        int alphaChnl;
        int redChnl;
        int greenChnl;
        int blueChnl;

        int page_id;
        std::string texture_file;

        int kernings = 0;

        std::map<int, BMFont_char> char_map;
        std::vector<BMFont_kerning> kerning_pairs;
    };

}