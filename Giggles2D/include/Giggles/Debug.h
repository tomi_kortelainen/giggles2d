#pragma once

#include <jni.h>
#include <errno.h>
#include <android/log.h>

#include <cstdarg>

namespace ggl {

    /**
    Namespace containing functions and macros useful to debugging.
    */
    namespace Debug {

        /**
        Importance level of the debug message.
        */
        enum class Level{
            ABORT = ANDROID_LOG_FATAL,
            ERROR = ANDROID_LOG_ERROR,
            WARNING = ANDROID_LOG_WARN,
            INFO = ANDROID_LOG_INFO
        };

        /**
        Print log message using androids log system.

        @param  tag     Importance level of the message.
        @param  message Message that can be formatted using c string formatting
                        used int printf for example.
        @param  ...     Values to print in message.
        */
        void Log(Level tag, const char* message, ...);

        /**
        Checks if OpenGL functions resulted in failure and prints message to
        the log if so.

        @param  tag Importance level of the message.
        @param  additional_message  Message to show in addition to the OpenGL
                                    error message.
        */
        void CheckGLError(Level tag, const char* additional_message);

    } /* namespace Debug */

} /* namespace ggl */