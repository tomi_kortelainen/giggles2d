#pragma once

namespace ggl {

    /**
    Singleton class to interface the touch input state.
    */
    class Touch
    {
    public:

        /**
        X coordinate of the touch input.
        */
        float x = 0.0f;

        /**
        Y coordinate of the touch input.
        */
        float y = 0.0f;

        /**
        True if touching the screen, false otherwise.
        */
        bool down = false;

        /**
        Get instance of Touch singleton.

        @return Instance of singleton object.
        */
        static Touch& instance()
        {
            static Touch touch;
            return touch;
        }

    private:
        // Singleton
        Touch(){};
        ~Touch(){};

        // Non-copyable
        Touch(const Touch&) = delete;
        void operator=(const Touch&) = delete;
    };

}