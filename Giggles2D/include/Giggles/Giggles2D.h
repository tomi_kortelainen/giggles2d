#pragma once

#include <jni.h>

#include "Giggles/Game/App.h"
#include "Giggles/Game/Scene.h"
#include "Giggles/Game/Object.h"

#include "Giggles/Graphics/Display.h"

#include "Giggles/Debug.h"

#include "Giggles/Components/Transform.h"
#include "Giggles/Components/Shape.h"
#include "Giggles/Components/Rectangle.h"
#include "Giggles/Components/Texture.h"
#include "Giggles/Components/RenderShape.h"

#include "Giggles/Resources/ResourceManager.h"



#include <EGL/egl.h>

namespace ggl {

    typedef glm::vec4 Color;

}