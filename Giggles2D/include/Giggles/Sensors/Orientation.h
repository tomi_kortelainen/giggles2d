#pragma once

namespace ggl {

    /**
    Singleton class to get info about the orientation state of the phone.
    Pitch, roll and yaw are available through public members.
    */

    class Orientation
    {
    public:

        float pitch = 0.0f;
        float roll = 0.0f;
        float yaw = 0.0f;

        /**
        Get instance of the singleton object.
        */
        static Orientation& instance()
        {
            static Orientation orientation;
            return orientation;
        }

    private:
        // Singleton
        Orientation(){};
        ~Orientation(){};

        // Non-copyable
        Orientation(const Orientation&) = delete;
        void operator=(const Orientation&) = delete;
    };

}