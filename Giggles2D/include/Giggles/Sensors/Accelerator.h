#pragma once

#include <glm/glm.hpp>

namespace ggl {
    /**
    Singleton class to get info about the accelerator state.
    Acceleration vector components are available throug public members x, y and z.
    */

    class Accelerator
    {
    public:

        float x = 0.0f;
        float y = 0.0f;
        float z = 0.0f;

        /**
        Returns normalized vector containing direction of the acceleration.
        */
        glm::vec3 getNormal();

        /**
        Returns inclination angle of the phone in radians.
        */
        float getInclination();

        /**
        Returns rotation angle of the phone in radians.
        */
        float getRotation();

        /**
        Get instance of the singleton object.
        */
        static Accelerator& instance()
        {
            static Accelerator accelerator;
            return accelerator;
        }

    private:
        // Singleton
        Accelerator() {};
        ~Accelerator() {};

        // Non-copyable
        Accelerator(const Accelerator&) = delete;
        void operator=(const Accelerator&) = delete;
    };

}