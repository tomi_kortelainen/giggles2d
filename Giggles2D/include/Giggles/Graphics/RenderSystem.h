#pragma once

#include <vector>

#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
//#include <glm/gtc/type_ptr.hpp>

#include <GLES2/gl2.h>

#include <Giggles/Resources/ShaderResource.h>

#include "Giggles/Debug.h"

namespace ggl {

    /**
    Singleton class containing interface to drawing and displaying 2D graphics
    on the display.
    */
    class RenderSystem
    {
    public:
        ShaderResource* custom_shader = nullptr;
        /**
        Clears display and sets its color.

        @param  r   Red component of the color.
        @param  g   Green component of the color.
        @param  b   Blue component of the color.
        @param  a   Alpha component of the color.
        */
        void clearColor(float r, float g, float b, float a);

        /**
        Clears display and sets its color.

        @param  color   Vector containing components of the color.
        */
        void clearColor(glm::vec4 color);

        /**
        Push vertex and index data to the GPU driver.

        @param  vertex_data Reference to the vector containing vertex data to
                            add.
        @param  index_data  Reference to the vector containing index data to
                            add.
        */
        void pushData(glm::mat4 current_transform,
             std::vector<float>& vertex_data,
             std::vector<unsigned short>& index_data);

        void setShader(ShaderResource& shad);

        /**
        Sets the texture to use for next draw calls.

        @param  texture_id   Id of the texture to use.
        */
        void setTexture(GLuint texture_id);

        /**
        Sets transformation to use for next draw calls.

        @param  transform   Transformation matrix to use.
        */
        //void pushTransform(glm::mat4 transform);

        /**
        Renders the screen and empties buffers.
        */
        void render();

        /**
        Get instance of this singleton object.

        @return Reference to the single instance of this object.
        */
        static RenderSystem& instance()
        {
            static RenderSystem rendersystem;
            return rendersystem;
        }
    private:

        ShaderResource* shader = nullptr;


        //std::vector<glm::mat4> transforms;
        //std::vector<glm::mat4> transform_buffer;

        std::vector<float> vertex_buffer;
        GLuint vertex_buffer_obj = 0;

        std::vector<unsigned short> index_buffer;
        GLuint index_buffer_obj = 0;

        GLuint current_texture = 0;



        unsigned short index_offset = 0;
        glm::mat4 projection;


        int color_attribute;
        int pos_attribute;
        int uv_attribute;

        const char* default_vertex_shader =
            "attribute mediump vec2 position;\n"
            "attribute mediump vec4 color;\n"
            "attribute mediump vec2 tex_coords;\n"

            "varying mediump vec4 out_color;\n"
            "varying mediump vec2 out_tex_coords;\n"

            "uniform mediump mat4 projection;\n"
            "//uniform mediump mat4 transform;\n"

            "void main(void)\n"
            "{\n"
            "    out_color = color;\n"
            "    out_tex_coords = tex_coords;\n"
            "    //gl_Position = projection * transform * vec4(position, 0.0, 1.0);\n"
            "    gl_Position = vec4(position, 0.0, 1.0);\n"
            "}\n";

        const char* default_fragment_shader =
            "varying mediump vec2 out_tex_coords;\n"
            "varying mediump vec4 out_color;\n"

            "uniform sampler2D texture;\n"

            "void main(void)\n"
            "{\n"
            "    gl_FragColor = texture2D(texture, out_tex_coords);\n"
            "    //gl_FragColor = out_color;\n"
            "}\n";

        // Singleton
        RenderSystem(){
            Debug::Log(Debug::Level::INFO, "RenderSystem initialized!");
            glEnable(GL_BLEND);
            glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

            //glEnable(GL_CULL_FACE);
            //glFrontFace(GL_CW);

            glEnable(GL_DEPTH_TEST);
            glDepthFunc(GL_LESS);

            int coordinates[4];
            glGetIntegerv(GL_VIEWPORT, coordinates);
            projection = glm::ortho((float)coordinates[0], (float)coordinates[2],
                (float)coordinates[3], (float)coordinates[1],
                -1.0f, 1.0f);

            shader = new ShaderResource();
            shader->load(default_vertex_shader, default_fragment_shader);
            shader->enable();
            shader->setProjection(projection);

            glGenBuffers(1, &vertex_buffer_obj);
            glGenBuffers(1, &index_buffer_obj);
        };
        ~RenderSystem(){
            glDeleteBuffers(1, &vertex_buffer_obj);
            glDeleteBuffers(1, &index_buffer_obj);
            delete shader;
        };

        // Non-copyable
        RenderSystem(const RenderSystem&) = delete;
        void operator=(const RenderSystem&) = delete;
    };

}