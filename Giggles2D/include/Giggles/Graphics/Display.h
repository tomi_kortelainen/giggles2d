#pragma once

#include <GLES2/gl2.h>
namespace ggl {

    /**
    Functions to initialize and query information about display.
    */
    namespace Display {

        namespace {
            int width;
            int height;
        }

        /**
        Initializes display and sets width and height. Gets called from Java
        after graphic context is created.

        @param  width   Width of the display in pixels.
        @param  height  Height of the display in pixels.
        */
        void init(int width, int height);

        /**
        Get width of the display.

        @return Width of the display.
        */
        int getWidth();

        /**
        Get height of the display.

        @return height of the display.
        */
        int getHeight();

        /**
        Get width of the display as float.

        @return Width of the display as float.
        */
        float getWidth_f();

        /**
        Get height of the display as float.

        @return height of the display as float.
        */
        float getHeight_f();
        
    };

} /* namespace ggl */