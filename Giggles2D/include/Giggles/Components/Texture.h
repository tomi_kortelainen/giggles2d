#pragma once
#include "Giggles/Components/Component.h"
#include <GLES2/gl2.h>

namespace ggl {

    /**
    Texture component. 
    */
    class Texture : public Component
    {
    friend class Scene;
    public:

        float width = 0.f;
        float height = 0.f;

        /**
        Method to get built in component type if any, otherwise
        BIType::NOT_SET.

        @return Type of the component as defined in BIType.
        */
        BIType getType() { return BIType::TEXTURE; }

        /**
        Returns identifier for texture stored in gpu.

        @return Identifier for texture.
        */
        GLuint getTexture();

        /**
        TODO: docs
        */
        float getTextureX(float pixelcoordx);
        float getTextureY(float pixelcoordy);
        
        /**
        Loads texture from file.

        @param  filename    Path to image file.
        */
        void load(const std::string& filename);

        /**
        Default constructor.
        */
        Texture();

        /**
        Constructor taking filename for texture file to load.

        @param  filename    Path to image file.
        */
        Texture(const std::string& filename);
        ~Texture();

    protected:
        GLuint texture_id = 0;
    };

} /* namespace ggl */
