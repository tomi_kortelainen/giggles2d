#pragma once

#include <string>

namespace ggl {

    /**
    Enum class containing possible types of built in components. Used mainly in
    Components type -member to tell engines systems to act accordingly.
    */
    enum class BIType
    {
        NOT_SET,
        TRANSFORM,
        SHAPE,
        RENDER_SHAPE,
        TEXTURE,
        TEXT,
        AUDIO,
        SHADER
    };

    class Component
    {
    public:
        /**
        Name of the component. Optional.
        */
        std::string name = "";

        /**
        Method to get built in component type if any, otherwise
        BIType::NOT_SET.

        @return Type of the component as defined in BIType.
        */
        BIType virtual getType() { return BIType::NOT_SET; }

        /**
        Default constructor.
        */
        Component();

        /**
        Constructor.

        @param  name    Name of the component.
        */
        Component(const std::string& name);
        virtual ~Component();
    };

} /* namespace ggl */