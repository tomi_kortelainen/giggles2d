#pragma once
#include "Giggles/Components/Component.h"
#include "Giggles/Resources/ShaderResource.h"

namespace ggl {

    class Shader : public Component
    {
    public:

        BIType getType() { return BIType::SHADER; }

        ShaderResource* shader = nullptr;
        // TODO: implement.
        /**
        Load shader from file. (not implemented...)

        @param  vert_filename   Path to the vertex shader.
        @param  frag_filename   Path to the fragment shader.
        @return Returns true if success, false otherwise.
        */
        bool load(const std::string& vert_filename,
            const std::string& frag_filename);

        /**
        Load shader from C style string in memory.

        @param  vert_ptr    Pointer to the vertex shader.
        @param  frag_ptr    Pointer to the fragment shader.
        */
        bool load(const char* vert_ptr, const char* frag_ptr);

        /**
        Sets texture to be used by shaders.

        @param  texid   Id of the texture to be used.
        */
        void setTexture(GLuint texid);

        /**
        Enables the shader.
        */
        void enable();

        // TODO: clean up! These are not currently needed. Should there be 
        // a more generic way to add uniforms?
        void setProjection(glm::mat4 proj);
        void setTransformation(glm::mat4 trans);

        /**
        Get the id of the shader program on the GPU.

        @return Id of the shader program.
        */
        GLuint getProgram();

        Shader();
        Shader(const std::string& vert_filename,
            const std::string& frag_filename);
        Shader(const char* vert_src, const char* frag_src);
        ~Shader();

    protected:
        
    };

}