#pragma once

#include "Giggles/Components/Component.h"

namespace ggl {

    /**
    Component that tells the engine that object is drawable.
    */
    class RenderShape : public Component
    {

    public:

        /**
        Method to get built in component type if any, otherwise
        BIType::NOT_SET.

        @return Type of the component as defined in BIType.
        */
        BIType getType() { return BIType::RENDER_SHAPE; }

        RenderShape();
        ~RenderShape();

    };
} /* namespace ggl */

