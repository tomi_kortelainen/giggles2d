#pragma once

#include <vector>

#include <glm/glm.hpp>

#include "Giggles/Components/Component.h"

#include "Giggles/Graphics/RenderSystem.h"

namespace ggl {

    /**
    Component representing 2d shape. Shape is stored raw in vectors.
    Vertices are stored as sequence of floats in format:
        x, y, red, green, blue, alpha, texture_coord_x, texture_coord_y
    Indices are stored as sequence of unsigned shorts.
    */
    class Shape : public Component
    {

    public:

        /**
        Method to get built in component type if any, otherwise
        BIType::NOT_SET.

        @return Type of the component as defined in BIType.
        */
        virtual BIType getType() { return BIType::SHAPE; }

        /**
        Default constructor
        */
        Shape();

        /**
        Destructor
        */
        virtual ~Shape();

        /**
        Vertices in format:
            x, y, red, green, blue, alpha, texture_coord_x, texture_coord_y
        */
        std::vector<float> vertices;

        /**
        Indices
        */
        std::vector<unsigned short> indices;
    };

} /* namespace ggl */
