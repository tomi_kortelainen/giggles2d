#pragma once
#include "Giggles/Components/Component.h"

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

namespace ggl {
    
    /**
    Identity matrix.
    */
    static const glm::mat4 identity = glm::mat4(1.0);

    /**
    Transform component. Contains position in 2D space, scale, rotation and
    origin point.
    */
    class Transform : public Component
    {
    public:

        /**
        2D vector representing position in 2D space.
        */
        glm::vec2 position = { 0.0f, 0.0f };

        /**
        2D vector representing scaling on the x-axis and y-axis.
        */
        glm::vec2 scale = { 1.0f, 1.0f };

        /**
        2D vector representing origin point from which rotation, position and 
        rotation are calculated.
        */
        glm::vec2 origin = { 0.0f, 0.0f };

        /**
        Rotation in radians.
        */
        float rotation = 0.0f;

        /**
        Method to get built in component type if any, otherwise
        BIType::NOT_SET.

        @return Type of the component as defined in BIType.
        */
        BIType getType() { return BIType::TRANSFORM; }

        /**
        Set rotation.

        @param  angle   Rotation in radians. Up is 0, increases to the right.
        */
        void setRotation(float angle);

        /**
        Set scale.

        @param  x   Scale in x-axis.
        @param  y   Scale in y-axis.
        */
        void setScale(float x, float y);
        
        /**
        Set position.

        @param  x   Position in x-axis.
        @param  y   Position in y-axis.
        */
        void setPosition(float x, float y);
        
        /**
        Set origin.

        @param  x   Origin in x-axis.
        @param  y   Origin in y-axis.
        */
        void setOrigin(float x, float y);

        /**
        Generates and returns transfrom matrix.

        @return Transform matrix.
        */
        glm::mat4 getTransform();

        /**
        Default constructor.
        */
        Transform();
        
        /**
        Constructor that sets position.

        @param  x   Position in x-axis.
        @param  y   Position in y-axis.
        */
        Transform(float x, float y);
        
        ~Transform();
    };

} /* namespace ggl */