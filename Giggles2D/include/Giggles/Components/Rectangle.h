#pragma once

#include "Giggles/Components/Shape.h"

namespace ggl {

    /**
    Component representing rectangle shape. Derived from Shape component.
    */
    class Rectangle : public Shape
    {
    public:

        glm::vec4 color = { 1.0f, 1.0f, 1.0f, 1.0f };
        
        /**
        Set width and height for the rectangle

        @param  width   New width.
        @param  height  New height.
        */
        void setDimensions(float width, float height);

        /**
        Set texture coordinates for the rectangle.

        @param  x   X coordinate of the position in texture.
        @param  y   Y coordinate of the position in texture.
        @param  width   Width of the selection.
        @param  height  Height of the selection.
        */
        void setTextureCoordinates(float x, float y, float width, float height);

        /**
        Default constructor. By default width and height are set to 1.0
        */
        Rectangle();

        /**
        Constructor setting width and height for the rectangle.

        @param  width   Width for the rectangle.
        @param  height  Height for the rectangle.
        */
        Rectangle(float width, float height);

        /**
        Constructor setting name for the rectangle component.

        @param  name    Name for the rectangle component.
        */
        Rectangle(const std::string& name);

        /**
        Constructor setting name, width and height for the rectangle component.

        @param  name    Name for the rectangle component.
        @param  width   Width for the rectangle.
        @param  height  Height for the rectangle.
        */
        Rectangle(const std::string& name, float width, float height);
        virtual ~Rectangle();

    private:
        /**
        Width of the rectangle
        */
        float width = 1.0f;

        /**
        Height of the rectangle
        */
        float height = 1.0f;

        float texture_x = 0.f;
        float texture_y = 0.f;
        float texture_w = 1.f;
        float texture_h = 1.f;

        /**
        Creates vertices for the rectangle.
        */
        void createVertices(float width, float height);
    };

} /* namespace ggl */
