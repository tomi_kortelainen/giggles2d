#pragma once

#include <jni.h>

#include <android/asset_manager_jni.h>
#include <android/asset_manager.h>


#include "Giggles/Graphics/Display.h"
#include "Giggles/Resources/ResourceManager.h"
#include "Giggles/Debug.h"

#include "Giggles/Internal/InputProcessor.h"

/**
Entrypoint of the program. Must be implemented by the user. Gets called when
OpenGL ES context is created all else is initialized and ready.
*/
void start();

/**
Update method of the program. Must be implemented byt the user. Gets called
at every frame.
*/
void update();

/**
Functions accessible from java.
*/
extern "C" {

    /**
    Initializes opengl state and gets usefull information about display.

    @param  env Java environment object.
    @param  obj Java object passed with native call. Not relevant to us.
    @param  width   Width of the screen.
    @param  height  Height of the screen.
    */
    JNIEXPORT void JNICALL Java_net_tomkort_giggles2d_Giggles2DJNILib_init(
        JNIEnv* env, jobject obj, jint width, jint height);

    /**
    Calls user defined update method at every frame.

    @param  env Java environment object.
    @param  obj Java object passed with native call. Not relevant to us.
    */
    JNIEXPORT void JNICALL Java_net_tomkort_giggles2d_Giggles2DJNILib_update(
        JNIEnv* env, jobject obj);

    /**
    Calls user defined start method after EGL context is created.

    @param  env Java environment object.
    @param  obj Java object passed with native call. Not relevant to us.
    */
    JNIEXPORT void JNICALL Java_net_tomkort_giggles2d_Giggles2DJNILib_start(
        JNIEnv* env, jobject obj);

    /**
    Gets pointer to Javas AssetManager object so it can be used with C++ code.

    @param  env Java environment object.
    @param  obj Java object passed with native call. Not relevant to us.
    @param  assetManager    AssetManager object from Java side.
    */
    JNIEXPORT void JNICALL Java_net_tomkort_giggles2d_Giggles2DJNILib_initAssetManager(
        JNIEnv* env, jobject obj, jobject assetManager);

    /**
    Updates touch state.

    @param  env Java environment object.
    @param  obj Java object passed with native call. Not relevant to us.
    @param  motionEvent MotionEven object from Java side.
    @param  down    True if screen is touched.
    */
    JNIEXPORT void JNICALL Java_net_tomkort_giggles2d_Giggles2DJNILib_setTouchState(
        JNIEnv* env, jobject obj, jobject motionEvent, jboolean down);

    /**
    Updates orientation state of the phone.

    @param  env Java environment object.
    @param  obj Java object passed with native call. Not relevant to us.
    @param  azimuth  Yaw.
    @param  pitch    Pitch.
    @param  roll     Roll.
    */
    JNIEXPORT void JNICALL Java_net_tomkort_giggles2d_Giggles2DJNILib_setOrientationState(
        JNIEnv* env, jobject obj, jfloat azimuth, jfloat pitch, jfloat roll);

    /**
    Updates accelerator state of the phone.

    @param  env Java environment object.
    @param  obj Java object passed with native call. Not relevant to us.
    @param  x   Acceleration in X-axis.
    @param  y   Acceleration in Y-axis.
    @param  z   Acceleration in Z-axis.
    */
    JNIEXPORT void JNICALL Java_net_tomkort_giggles2d_Giggles2DJNILib_setAccelerationState(
        JNIEnv* env, jobject obj, jfloat x, jfloat y, jfloat z);
}

JNIEXPORT void JNICALL Java_net_tomkort_giggles2d_Giggles2DJNILib_init(
    JNIEnv* env, jobject obj, jint width, jint height)
{
    ggl::Display::init(width, height);
}

JNIEXPORT void JNICALL Java_net_tomkort_giggles2d_Giggles2DJNILib_update(
    JNIEnv* env, jobject obj)
{
    update();
}

JNIEXPORT void JNICALL Java_net_tomkort_giggles2d_Giggles2DJNILib_start(
    JNIEnv* env, jobject obj)
{
    start();
}

JNIEXPORT void JNICALL Java_net_tomkort_giggles2d_Giggles2DJNILib_initAssetManager(
    JNIEnv* env, jobject obj, jobject assetManager)
{
    ggl::ResourceManager::instance().asset_manager = AAssetManager_fromJava(env, assetManager);
}

JNIEXPORT void JNICALL Java_net_tomkort_giggles2d_Giggles2DJNILib_setTouchState(
    JNIEnv* env, jobject obj, jobject motionEvent, jboolean down)
{
    ggl::priv::processTouch(env, motionEvent, down);
}

JNIEXPORT void JNICALL Java_net_tomkort_giggles2d_Giggles2DJNILib_setOrientationState(
    JNIEnv* env, jobject obj, jfloat azimuth, jfloat pitch, jfloat roll)
{
    ggl::priv::processOrientation(env, azimuth, pitch, roll);
}

JNIEXPORT void JNICALL Java_net_tomkort_giggles2d_Giggles2DJNILib_setAccelerationState(
    JNIEnv* env, jobject obj, jfloat x, jfloat y, jfloat z)
{
    ggl::priv::processAcceleration(env, x, y, z);
}