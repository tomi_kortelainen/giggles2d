#pragma once

#include <jni.h>

#include "Giggles/Debug.h"
#include "Giggles/Input/Touch.h"
#include "Giggles/Sensors/Orientation.h"
#include "Giggles/Sensors/Accelerator.h"


namespace ggl {

    /**
    Namespace containing engines internal functions not meant to be used from
    outside.
    */
    namespace priv {

        /**
        Takes MotionEven object from java and passes relevant data to engines
        touch state object.

        @param  env     JNI environment object.
        @param  obj     MotionEvent object from java.
        @param  down    True if touched, false otherwise.
        */
        void processTouch(JNIEnv* env, jobject obj, jboolean down)
        {
            ggl::Touch& touch = ggl::Touch::instance();
            bool touched = false;
            float x = 0.0f;
            float y = 0.0f;

            touched = (bool)(down == JNI_TRUE);
            x = env->CallFloatMethod(obj, env->GetMethodID(env->GetObjectClass(obj), "getX", "()F"));
            y = env->CallFloatMethod(obj, env->GetMethodID(env->GetObjectClass(obj), "getY", "()F"));
            touch.x = x;
            touch.y = y;
            touch.down = touched;

        }

        /**
        Takes SensorEvent object from Java and passes orientation state to
        the engine.

        @param  env JNI environment object.
        @param  obj SensorEvent object.
        */
        void processOrientation(JNIEnv* env, jfloat azimuth, jfloat pitch, jfloat roll)
        {
            ggl::Orientation& orientation = ggl::Orientation::instance();

            orientation.yaw = - azimuth * 3.141f / 180.0f;
            orientation.pitch = pitch * 3.141f / 180.0f;
            orientation.roll = roll * 3.141f / 180.0f;
        }

        void processAcceleration(JNIEnv* env, jfloat x, jfloat y, jfloat z)
        {
            ggl::Accelerator& accelerator = ggl::Accelerator::instance();

            accelerator.x = x;
            accelerator.y = y;
            accelerator.z = z;
        }

    }
}