#pragma once

#include <string>

#include <SLES/OpenSLES.h>
#include <SLES/OpenSLES_Android.h>

#include <Giggles/Resources/AudioResource.h>
#include <Giggles/Resources/ResourceManager.h>

namespace ggl {

    /**
    Interface to play audio files.
    */
    class AudioObject
    {
        public:

            /**
            Play audio.
            */
            bool play();

            /**
            Pause audio.
            */
            bool pause();

            /**
            Stop audio and reset play pointer to beginning.
            */
            bool stop();

            /**
            Create new audio object.

            @param  filename    Path to the file to open.
            @param  loop        Bool to tell if audio is played in loop. For
                                example background music.
            @return Pointer to the created audio object.
            */
            static AudioObject* create(const std::string& filename, bool loop);

            ~AudioObject();
       
        protected:

            bool playing = false;

            // Engine interface
            SLObjectItf engine_obj = nullptr;
            SLEngineItf engine = nullptr;

            // Output interface
            SLObjectItf output_mix_obj = nullptr;
          
            // Asset player
            SLObjectItf player_obj = nullptr;
            SLPlayItf player_play = nullptr;
            SLSeekItf player_seek = nullptr;
            SLMuteSoloItf player_mutesolo = nullptr;
            SLVolumeItf player_volume = nullptr;

            AudioObject(){};
            
    };

} /* namespace ggl */
