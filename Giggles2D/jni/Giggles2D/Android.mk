LOCAL_PATH := $(call my-dir)

FILE_LIST := $(wildcard $(LOCAL_PATH)/../../src/*.cpp) \
	$(wildcard $(LOCAL_PATH)/../../src/**/*.cpp)

include $(CLEAR_VARS)

LOCAL_MODULE := libGiggles2D
LOCAL_CFLAGS := -Wall -Wno-extern-c-compat
LOCAL_SRC_FILES := $(FILE_LIST:$(LOCAL_PATH)/%=%)

LOCAL_C_INCLUDES += include/
LOCAL_C_INCLUDES += ext/glm/ ext/stb/
LOCAL_C_INCLUDES += $(NDK_ROOT)/sources/cxx-stl/llvm-libc++/libcxx/include/

include $(BUILD_STATIC_LIBRARY)
