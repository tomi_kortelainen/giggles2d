package net.tomkort.giggles2d;

import android.content.Context;
import android.hardware.GeomagneticField;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Criteria;
import android.location.GpsSatellite;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;

public class Giggles2DSensors implements SensorEventListener {
    private static String TAG = "GigglesSensor";

    public float azimuth;
    public float pitch;
    public float roll;

	public float accelX;
	public float accelY;
	public float accelZ;

    Context ctx;
    SensorManager sensorManager;

    public Giggles2DSensors(Context context)
    {
        ctx = context;
        sensorManager = (SensorManager)ctx.getSystemService(ctx.SENSOR_SERVICE);
        sensorManager.registerListener(this,
            sensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION),
            SensorManager.SENSOR_DELAY_UI);
		sensorManager.registerListener(this,
            sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
            SensorManager.SENSOR_DELAY_UI);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        switch(event.sensor.getType()) {
        // TODO: TYPE_ORIENTATION is deprecated
        case Sensor.TYPE_ORIENTATION:
        {
            azimuth = event.values[0];
            pitch = event.values[1];
            roll = event.values[2];
            //Log.v(TAG, "az: " + azimuth + " pitch: " + pitch + " roll: " + roll);
            break;
        }
		case Sensor.TYPE_ACCELEROMETER:
		{
			accelX = event.values[0];
			accelY = event.values[1];
			accelZ = event.values[2];
			//Log.v(TAG, "X: " + accelX + " Y: " + accelY + " Z: " + accelZ);
			break;
		}
		default:
			break;
        }
    }

}
