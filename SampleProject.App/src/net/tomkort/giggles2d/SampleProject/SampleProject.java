
package net.tomkort.giggles2d.SampleProject;

import net.tomkort.giggles2d.Giggles2DView;

import android.app.Activity;
import android.os.Bundle;

/**
 * This class loads the Java Native Interface (JNI)
 * library, 'libSampleProject.so', and provides access to the
 * exposed C functions.
 * The library is packaged and installed with the application.
 * See the C file, /jni/SampleProject.c file for the
 * implementations of the native methods. 
 * 
 * For more information on JNI, see: http://java.sun.com/docs/books/jni/
 */

public class SampleProject extends Activity
{

	Giggles2DView gglView;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		gglView = new Giggles2DView("SampleProject", getApplication());
		setContentView(gglView);
	}

	@Override
	protected void onPause()
	{
		System.exit(0);
	}

}
