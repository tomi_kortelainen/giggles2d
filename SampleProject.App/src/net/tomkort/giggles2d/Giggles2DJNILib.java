package net.tomkort.giggles2d;

import android.content.res.AssetManager;
import android.view.MotionEvent;
import android.hardware.SensorEvent;

public class Giggles2DJNILib {
	/*static {
		System.loadLibrary("SampleProject");
	}*/

	public static void loadNativeLibrary(String name)
	{
		System.loadLibrary(name);
	}
	
    public static native void init(int width, int height);
    public static native void update();
	public static native void start();
    public static native void initAssetManager(AssetManager assetManager);

    public static native void setTouchState(MotionEvent motionEvent, boolean down);
    public static native void setOrientationState(float azimuth, float pitch, float roll);
	public static native void setAccelerationState(float x, float y, float z);
}
