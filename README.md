# README #

Giggles2D is a 2D game engine for Android API 15+ written in C++. It's not yet feature complete. Made as a school project which has ended. I might get back to this later but as of now there is no development.

## Features ##

* Hardware accelerated graphics using OpenGL ES 2.0
* Audio with OpenSL ES
* Acceleration sensor
* Orientation sensor
* Easily integrated with other Java based views

## Requirements ##

* Visual Studio 2015 with native Android development tools
* Git client (I'm using Atlassians [SourceTree](https://www.sourcetreeapp.com/)

## Usage ##

* Clone this repository (https://tomi_kortelainen@bitbucket.org/tomi_kortelainen/giggles2d.git) to a folder that contains no spaces in its name.
* Remember to initialize and update submodules in the "Giggles2D\ext" folder.
* Open "Giggles2D.sln" VS2015 solution in the root folder.
* Solution contains three projects:
    * **Giggles2D** contains the core engine which compiles to static library.
    * **SampleProject** contains the native c++ code for the sample project. It uses the Giggles2D engine and compiles to dynamic library to be used from Java side of the application.
    * **SampleProject.App** contains the Java code of the application. It also contains the Giggles2DView and the class to interface with the native side of the application.

## Links ##

* [Documentation](http://tomi_kortelainen.bitbucket.org/Giggles2D/)