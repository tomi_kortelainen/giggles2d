﻿#pragma once

#include <Giggles/Giggles2D.h>
#include <Giggles/Game/Sprite.h>
#include <Giggles/Game/GUI/Text.h>
#include <Giggles/Audio/AudioObject.h>
#include <Giggles/Sensors/Orientation.h>
#include <Giggles/Sensors/Accelerator.h>
#include <Giggles/Game/Object.h>

class StartMenu : public ggl::Scene
{
public:

    ggl::Orientation& orientation = ggl::Orientation::instance();
    ggl::Accelerator& acceleration = ggl::Accelerator::instance();

    std::vector<ggl::Sprite*> objects;
    ggl::Sprite* player;
    ggl::Text* text;

    ggl::AudioObject* background;

    ggl::Object* test;

    float angle = 0.0f;

    float scale = 1.0f;
    bool sub = false;

    void update();

    StartMenu();
    ~StartMenu();
};

