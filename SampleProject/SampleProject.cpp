﻿
/**
Entrypoint for game code!
*/

#include <Giggles/Start.h>
#include <Giggles/Giggles2D.h>

#include "StartMenu.h"

ggl::App app;
/**
Must be implemented! This is the first user code that is run after graphics
context is created.
*/
void start()
{    
    app.startScene(new StartMenu());
    app.run();
}

void update()
{
    app.update();
}
