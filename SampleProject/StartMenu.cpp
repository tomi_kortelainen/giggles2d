﻿
#include "Giggles/Giggles2D.h"

#include "StartMenu.h"

#include <string>
#include <cmath>
#include <cstdlib>


void StartMenu::update()
{
    player->transform.setRotation(acceleration.getRotation());
    if (touch.down) {
        player->transform.setPosition(touch.x, touch.y);
    }


    if (angle > (2.0f * 3.141f)) {
        angle = 0.0f;
    }
    angle += 0.05f;

    if (scale > 2)
        sub = true;
    if (scale < 0.2)
        sub = false;

    if (sub)
        scale -= 0.05f;
    else
        scale += 0.05f;

    for (auto&& obj : objects) {
        obj->transform.setRotation(angle);
        obj->transform.setScale(scale, scale);
    }

    Scene::update();

}

StartMenu::StartMenu()
{
    background = ggl::AudioObject::create("background.mp3", true);
    background->play();
    for (int i = 0; i < 100; i++)
    {
        objects.push_back((ggl::Sprite*)addObject(new ggl::Sprite()));

        objects.back()->transform.setPosition(
            std::rand() % (ggl::Display::getWidth() - 20),
            std::rand() % (ggl::Display::getHeight() - 20));
        objects.back()->transform.setOrigin(20.0f, 20.0f);
        objects.back()->rectangle.setDimensions(40.0f, 40.0f);

        if ((std::rand() % 100) < 50) {

            objects.back()->texture.load("heart.png");
        }
        else {

            objects.back()->texture.load("Xubuntu.png");
        }
    }

    player = (ggl::Sprite*)addObject(new ggl::Sprite());
    player->transform.setOrigin(20.0f, 20.0f);
    player->transform.setPosition(
        (float)ggl::Display::getWidth() / 2.0f,
        (float)ggl::Display::getHeight() / 2.0f);
    player->rectangle.setDimensions(40.0f, 40.0f);
    player->transform.setScale(4.0f, 4.0f);
    player->texture.load("heart.png");
    
    text = (ggl::Text*)addObject(new ggl::Text("arial.fnt", L"Tj fj öäöäöäö"));
    text->transform.setPosition(80.f, 200.f);
    text->updateText();
}

StartMenu::~StartMenu()
{
    
}
