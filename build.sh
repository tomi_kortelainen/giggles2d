#!/bin/bash

# Set correct path to Android NDK
NDK_PATH=~/AndroidDev/android-ndk-r10e

cd Giggles2D && \
${NDK_PATH}/ndk-build && \
cd ../SampleProject && \
${NDK_PATH}/ndk-build && \
cp -r libs ../SampleProject.App && \
cd ../SampleProject.App && \
ant debug && \
cd ..
